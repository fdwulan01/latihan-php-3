<?php

class Animal{
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    public function __construct($string){
        $this->name = $string;
    }

    public function get_name(){
        echo "Nama hewan : $this->name <br>";
    }

    public function get_legs(){
        echo "Jumlah kaki : $this->legs <br>";
    }

    public function get_cold_blooded(){
        echo "Berdarah dingin : $this->cold_blooded <br>";
    }
}

?>